#include "image.h"
#include <stdlib.h>

Image create_image(size_t width, size_t height) {
  Image image = (Image){.width = width, .height = height};
  image.pixels = malloc(width * height * sizeof(struct pixel));
  image.valid = image.pixels != NULL;
  return image;
}

bool is_image_invalid(Image image) {
  return !image.valid;
}

void destroy_image(Image target) { free(target.pixels); }

size_t get_size(Image image) {
  return sizeof(struct pixel) * image.width * image.height;
}

struct pixel get_pixel(Image source, size_t width_pos, size_t height_pos) {
  return source.pixels[width_pos * source.width + height_pos];
}

void set_pixel(Image const target, struct pixel pixel, size_t width_pos,
               size_t height_pos) {
  target.pixels[target.width * width_pos + height_pos] = pixel;
}
