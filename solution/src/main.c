#include "bmp/bmp_reader.h"
#include "bmp/bmp_writer.h"
#include "bmp/rw_status_codes.h"
#include "file/file_io.h"
#include "format/image.h"
#include "transform/rotate.h"
#include "util/util.h"
#include <stdio.h>

int main(int argc, char **argv) {
  if (argc != 3) {
    println_err("Wrong arguments format; use: `image-transformer <source "
                "image> <output file>`");
    return 1;
  }

  const char *const source_file_name = argv[1];
  const char *const output_file_name = argv[2];
  FILE *source_file;

  struct image source_image = {0};

  if (open_file(&source_file, source_file_name, "rb") == OPEN_ERROR) {
    return 1;
  }
  enum read_status result_status = read_bmp_image(&source_image, source_file);
  file_close(source_file);
  if (result_status != READ_OK) {
    destroy_image(source_image);
    println_err(read_error_messages[result_status]);
    println_err("Error occurred while reading source image. Exiting!");
    return 1;
  }

  const struct image rotated_image = rotate(source_image);
  destroy_image(source_image);
  if (is_image_invalid(rotated_image)) {
    destroy_image(rotated_image);
    println_err("Corrupted image. Exiting!");
    return 1;
  }

  FILE *output_file;
  if (open_file(&output_file, output_file_name, "wb") == OPEN_ERROR) {
    destroy_image(rotated_image);
    return 1;
  }

  enum write_status write_image_status =
      write_bmp_image(rotated_image, output_file);
  file_close(output_file);

  destroy_image(rotated_image);
  if (write_image_status != WRITE_OK) {
    println_err(write_error_messages[result_status]);
    println_err("Error occurred while writing rotated image. Exiting!");
    return 1;
  }

  println("Image is rotated");
  return 0;
}
