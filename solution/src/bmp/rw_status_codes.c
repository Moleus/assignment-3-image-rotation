#include "rw_status_codes.h"

const char *const read_error_messages[] = {
    [READ_ERR] = "Failed to read image",
    [READ_INVALID_HEADER] = "Invalid file header",
    [READ_INVALID_SIGNATURE] = "Invalid signature",
    [READ_INVALID_BITS] = "Invalid bits"};

const char *const write_error_messages[] = {
    [WRITE_ERR] = "Failed to write image",
    [WRITE_ERR_HEADER] = "Failed to write header",
    [WRITE_ERR_PIXELS] = "Failed to write pixels",
    [WRITE_ERR_PADDING] = "Failed to write padding"};
