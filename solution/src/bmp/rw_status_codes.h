#pragma once

enum read_status {
  READ_OK = 0,
  READ_ERR,
  READ_INVALID_HEADER,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS
};

extern const char *const read_error_messages[];

enum write_status {
  WRITE_OK = 0,
  WRITE_ERR,
  WRITE_ERR_HEADER,
  WRITE_ERR_PIXELS,
  WRITE_ERR_PADDING
};

extern const char *const write_error_messages[];
