#pragma once

#include "../format/image.h"
#include "rw_status_codes.h"
#include <stdio.h>

enum read_status read_bmp_image(Image *target, FILE *file);
