#include "bmp_reader.h"
#include "bmp_header.h"
#include "bmp_padding.h"
#include "rw_status_codes.h"
#include <stdio.h>

static enum read_status read_header(struct bmp_header *target, FILE *file);

static enum read_status read_pixels(Image image, FILE *file);

enum read_status read_bmp_image(Image *const target, FILE *const file) {
  if (target == NULL || file == NULL) return READ_ERR;
  struct bmp_header header = {0};
  enum read_status header_status = read_header(&header, file);
  if (header_status != READ_OK)
    return header_status;

  *target = create_image(header.biWidth, header.biHeight);
  if (is_image_invalid(*target)) return READ_ERR;
  return read_pixels(*target, file);
}

static enum read_status read_header(struct bmp_header *const target,
                                    FILE *const file) {
  if (fread(target, sizeof(struct bmp_header), 1, file) != 1)
    return READ_INVALID_HEADER;
  return READ_OK;
}

static enum read_status read_pixels(const Image image, FILE *const file) {
  const size_t height = image.height;
  const size_t width = image.width;
  struct pixel *const pixels = image.pixels;

  const uint8_t padding = get_padding(width);
  for (size_t i = 0; i < height; i++) {
    if (fread(pixels + width * i, sizeof(struct pixel), width, file) != width)
      return READ_INVALID_SIGNATURE;
    // skip padding
    if (fseek(file, padding, SEEK_CUR) != 0)
      return READ_INVALID_BITS;
  }
  return READ_OK;
}
