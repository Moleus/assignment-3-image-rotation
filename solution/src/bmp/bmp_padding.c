#include "bmp_padding.h"
#include "../format/image.h"

#define ALIGN_BYTES 4

uint8_t get_padding(const uint32_t image_width) {
  const uint8_t mod = image_width * sizeof(struct pixel) % ALIGN_BYTES;
  return mod ? ALIGN_BYTES - mod : mod;
}
