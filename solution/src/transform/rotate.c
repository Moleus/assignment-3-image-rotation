
#include "../format/image.h"
#include "stdlib.h"

struct image rotate(Image source) {
  size_t rotated_height = source.width;
  size_t rotated_width = source.height;
  Image target = create_image(rotated_width, rotated_height);
  if (is_image_invalid(target)) return target;

  for (size_t height = 0; height < source.height; height++) {
    for (size_t width = 0; width < source.width; width++) {
      struct pixel old_pixel = get_pixel(source, height, width);
      set_pixel(target, old_pixel, width, target.width - height - 1);
    }
  }
  return target;
}
