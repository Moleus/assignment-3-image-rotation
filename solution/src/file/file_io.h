#pragma once

#include "../format/image.h"
#include "io_status_codes.h"
#include <stdio.h>

// wraps fopen call, displays error
enum open_status open_file(FILE **result_file, const char *file_name,
                           const char *mode);

// wraps fclose call, displays error
enum close_status file_close(FILE *file);
